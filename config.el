(setq bookmark-default-file "~/.config/doom/bookmarks")

(map! :leader
      (:prefix ("b". "buffer")
       :desc "List bookmarks"                          "L" #'list-bookmarks
       :desc "Set bookmark"                            "m" #'bookmark-set
       :desc "Delete bookmark"                         "M" #'bookmark-set
       :desc "Save current bookmarks to bookmark file" "w" #'bookmark-save))

;;(setq scroll-preserve-screen-position t
;;      scroll-conservatively 0
;;      maximum-scroll-margin 0.5
;;      scroll-margin 99999)

(setq doom-fallback-buffer "*dashboard*")
;;(setq fancy-splash-image "~/.doom.d/Dark_Souls_Logo.png")  ;; use custom image as banner

(setq delete-by-moving-to-trash t
      trash-directory "~/.local/share/Trash/files/")

(setq doom-font (font-spec :family "Hack" :size 16)
      doom-variable-pitch-font (font-spec :family "Hack" :size 16)
      doom-big-font (font-spec :family "Hack" :size 24)
      doom-serif-font (font-spec :family "Liberation Serif" :size 16))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(map! :leader
      :desc "Toggle LaTeX pdf preview pane" "t p" #'latex-preview-pane-mode)

(setq display-line-numbers-type 'relative)
;; Make movement keys work like they should
(define-key evil-normal-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
(define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
(define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
(define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
; Make horizontal movement cross lines
(setq-default evil-cross-lines t)

(setq doom-modeline-height 30     ;; sets modeline height
      doom-modeline-bar-width 5   ;; sets right bar width
      doom-modeline-icon (display-graphic-p)
      doom-modeline-modal-icon nil
      doom-modeline-buffer-state-icon t
      doom-modeline-persp-name t  ;; adds perspective name to modeline
      doom-modeline-persp-icon t) ;; adds folder icon next to persp name

(after! neotree
  (setq neo-smart-open t
        neo-window-fixed-size nil))
(after! doom-themes
  (setq doom-neotree-enable-variable-pitch t))
(map! :leader
      :desc "Toggle neotree file viewer" "t n" #'neotree-toggle
      :desc "Open directory in neotree" "d n" #'neotree-dir)

(setq org-directory "~/org/")

(map! :leader
      :desc "Org babel tangle" "m B" #'org-babel-tangle)

(add-hook! 'org-mode-hook #'mixed-pitch-mode)

;; Make Org headers prettier
(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.4))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.3))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.1))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
)

(define-globalized-minor-mode global-rainbow-mode rainbow-mode
  (lambda ()
    (when (not (memq major-mode
                (list 'org-agenda-mode)))
     (rainbow-mode 1))))
(global-rainbow-mode 1 )

(setq shell-file-name "/bin/zsh"
      vterm-max-scrollback 5000)
(map! :leader
      :desc "Vterm popup toggle" "v t" #'+vterm/toggle)

(setq doom-theme 'doom-gruvbox)
(map! :leader
      :desc "Load new theme" "h t" #'load-theme)

;; Praise the sun
(solaire-global-mode +1)

(setq user-full-name "Dustin Morse"
      user-mail-address "dustymorse@gmail.com")
